/**
 * Saves coordinates of a pixel which will be turned on at solution display.
 *
 * @param x value of x coordinate
 * @param y value of y coordinate
 * @param res object to save pixel which needs to be turned on
 */
function savePixel(x, y, res) {
    res.selectedPixels[Math.round(y)][Math.round(x)] = 1;
}


/**
 * Saves value of error for pixel defined at algorithm level.
 *
 * @param x value of pixel horizontal position
 * @param y value of pixel vertical position
 * @param value value to set
 * @param res object to save the value to
 */
function setPixelValue(x, y, value, res) {
    if (value === 0) {
        res.pixelValues[y][x] = 0;
    } else {
        res.pixelValues[y][x] = value;
    }

}


/**
 * Inits bresenham algorithm.
 *
 * @param correct data which will is used to save pixels selection and error values to
 * @param xs value for start x coordinate
 * @param ys value for start y coordinate
 * @param xe value for end x coordinate
 * @param ye value for end y coordinate
 * @returns {*} 2D array of saved data for specified line segment
 */
function bresenhamAlgo(correct, xs, ys, xe, ye) {
    if (xs <= xe) {
        if (ys <= ye) {
            correct = bresenhamStd(xs, ys, xe, ye, correct);
        }
        else {
            correct = bresenhamNegativeStd(xs, ys, xe, ye, correct);

        }
    }
    return correct;
}


/**
 * Standard bresenham algorithm with decimal numbers.
 *
 * @param xs value for start x coordinate
 * @param ys value for start y coordinate
 * @param xe value for end x coordinate
 * @param ye value for end y coordinate
 * @param res object representing correct question results
 * @returns {*}  2D array of saved data for specified line segment
 */
function bresenhamStd(xs, ys, xe, ye, res) {
    let dx = xe - xs;
    let dy = ye - ys;
    let tg = dy / dx;
    let D = 0;
    let y = ys;

    for (let x = xs; x < xe; x++) {
        savePixel(x, y, res);
        setPixelValue(x, y, D, res);
        D += tg;
        if (D >= 0.5) {
            y = y + 1;
            D = D - 1;
        }
    }
    return res;
}


/**
 *  Standard Bresenham but for negative slopes.
 *
 * @param xs value for start x coordinate
 * @param ys value for start y coordinate
 * @param xe value for end x coordinate
 * @param ye value for end y coordinate
 * @param res object representing correct question results
 * @returns {*}  2D array of saved data for specified line segment
 */
function bresenhamNegativeStd(xs, ys, xe, ye, res) {
    let dx = xe - xs;
    let dy = ye - ys;
    let tg = dy / dx;
    let D = 0;
    let y = ys;

    for (let x = xs; x < xe; x++) {
        savePixel(x, y, res);
        setPixelValue(x, y, D, res);
        D += tg;
        if (D <= -0.5) {
            y = y - 1;
            D = D + 1.0;
        }
    }
    return res;
}