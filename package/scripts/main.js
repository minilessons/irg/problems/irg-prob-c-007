/*@#include#(serverUtil.js)*/
/*@#include#(bresenhamAlgo.js)*/
/*@#common-include#(commonUtil.js)*/


/* Main Module
 * Contains backend functions which server side calls in order to setup and evaluate task; it
 * also deals with data stream between frontend and backend.
 */

/**
 * Initializer functions for question. Uses configuration file for setting some variables.
 *
 * @param questionConfig configuration JSON object provided in "config-default.json" file.
 */
function questionInitialize(questionConfig) {
    // init task configuration (see: config.json file for details)
    let rows = questionConfig.rows;
    let cols = questionConfig.cols;

    let pixelWidth = questionConfig.pixelWidth;
    let pixelHeight = questionConfig.pixelHeight;
    let pixelWorth = questionConfig.pixelWorth;
    let pixelValueWorth = questionConfig.pixelValueWorth;
    let negativePixelWorth = questionConfig.negativePixelWorth;

    let roundTo = questionConfig.pixelValuePrecision;
    let evalPrecision = questionConfig.evalPrecision;

    let correctState = {
        selectedPixels: init2DArray(rows, cols, 0),
        pixelValues: init2DArray(rows, cols, "")
    };

    // Generating points and solution
    let points = generatePoints(rows, cols);
    let correct = bresenhamAlgo(correctState,
        points.x0, points.y0, points.x1, points.y1);

    correct.pixelValues = setPrecision(correct.pixelValues, roundTo);

    // Saving information for question and other data which will be sent to front-end
    /**
     * @external userData
     *
     */
    userData.rasterSpecs = {
        rows: rows,
        cols: cols,
        pixelWidth: pixelWidth,
        pixelHeight: pixelHeight,
        canWidth: questionConfig.canWidth,
        canHeight: questionConfig.canHeight
    };

    userData.evalOpts = {
        pixelWorth: pixelWorth,
        pixelValueWorth: pixelValueWorth,
        negativePixelWorth: negativePixelWorth,
        evalPrecision: evalPrecision
    };

    userData.question = {
        points: points,
        correct: correct,
        questionState: {
            selectedPixels: init2DArray(rows, cols, 0),
            pixelValues: init2DArray(rows, cols, "")
        }
    };
}


/**
 * Returns computed properties which can be used in frontend as echo directives
 *
 * @returns {{x0, y0: *, x1, y1: *}} coordinates of question start point and end point
 */
function getComputedProperties() {
    return {
        x0: userData.question.points.x0,
        y0: userData.question.points.y0,
        x1: userData.question.points.x1,
        y1: userData.question.points.y1
    };
}


/**
 * Question evaluator for this question.
 *
 * @returns {{correctness: number, solved: boolean}}
 */
function questionEvaluate() {

    /**
     * Sets correct solution if user did not answer the question 100%.
     */
    function setCorrectSolution() {
        userData.correctQuestionState = userData.question.correct;
    }


    let res = {correctness: 0.0, solved: false};
    userData.question.solved = compareStates(exportEmptyState().qs, userData.question.questionState);

    if (userData.question.solved === false) {
        setCorrectSolution();
        return res;
    }

    res.solved = true;

    let qs = userData.question.questionState;
    let cqs = userData.question.correct;
    let cols = userData.rasterSpecs.cols;
    let rows = userData.rasterSpecs.rows;

    // Messure is:
    let maxPixels = userData.evalOpts.pixelWorth;
    let maxValues = userData.evalOpts.pixelValueWorth;

    // Each pixel is worth:
    let numOfCols = cols - 2;
    let pixelWorth = maxPixels / numOfCols;
    let valueWorth = maxValues / numOfCols;

    // Extra pixels are negative points
    let extraPixelWorth = userData.evalOpts.negativePixelWorth;

    let extraPixels = 0;
    let correctPixels = 0;
    let correctValues = 0;
    let precisionCheck = userData.evalOpts.evalPrecision;
    for (let c = 0; c < rows; c++) {
        for (let k = 0; k < cols; k++) {
            if (qs.selectedPixels[c][k] === 1) {
                if ((qs.pixelValues[c][k] !== "")
                    && isCorrect(
                        parseFloat(
                            qs.pixelValues[c][k]),
                        cqs.pixelValues[c][k],
                        precisionCheck)) {
                    ++correctValues;
                }
                if (cqs.selectedPixels[c][k] === 1) {
                    ++correctPixels;
                } else {
                    ++extraPixels;
                }
            }
        }
    }

    let extraPixelsWorth = extraPixelWorth * extraPixels;
    let finalGrade = correctPixels * pixelWorth +
        correctValues * valueWorth - extraPixelsWorth;

    if (finalGrade < 0) {
        res.correctness = 0;
        setCorrectSolution();
    } else if (Math.abs(finalGrade - 1) < precisionCheck) {
        userData.correctQuestionState = userData.question.questionState;
        res.correctness = finalGrade;
    } else {
        setCorrectSolution();
        res.correctness = finalGrade;
    }

    return res;
}


/**
 * Exports user state and question information which frontend basic question setup needs
 *
 * @returns {{points: ({x0, x1, y0, y1}|*), rows: *, cols: *, questionState: {selectedPixels: *, pixelValues: *}}}
 */
function exportCommonState() {
    return {
        points: userData.question.points,
        rasterSpecs: userData.rasterSpecs,
        questionState: userData.question.questionState
    };
}


/**
 * Exports question state depending on type asked
 *
 * @param stateType can be user or correct solution
 * @returns {*}
 */
function exportUserState(stateType) {
    if (stateType === "USER") {
        return {qs: userData.question.questionState};
    }
    if (stateType === "CORRECT") {
        return {qs: userData.correctQuestionState};
    }

    return {};
}


/**
 * Exports empty state of user data question state
 *
 * @returns {{qs: {selectedPixels: *, pixelValues: *}}}
 */
function exportEmptyState() {
    return {
        qs: {
            selectedPixels: init2DArray(
                userData.rasterSpecs.rows, userData.rasterSpecs.cols, 0),
            pixelValues: init2DArray(
                userData.rasterSpecs.rows, userData.rasterSpecs.cols, "")
        }
    };
}


/**
 * Imports question state data which is changed by the user in front end and saves it for Database.
 *
 * @param data question state data; question state should be of the same form as exported one
 */
function importQuestionState(data) {
    userData.question.questionState = data;
}