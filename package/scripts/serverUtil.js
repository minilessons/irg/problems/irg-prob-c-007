/**
 * Compares two states for equality.
 *
 * @param p first state
 * @param p2 second state
 * @returns {boolean} true if equal
 */
function compareStates(p, p2) {
    let pix1 = p.selectedPixels;
    let pix2 = p2.selectedPixels;
    let vals1 = p.pixelValues;
    let vals2 = p2.pixelValues;
    let rows = pix1.length;
    let cols = pix1[0].length - 1;

    // Compare pixels and values
    let fEqual = compare2DArrays(pix1, pix2, rows, cols, 0, 1);
    let sEqual = compare2DArrays(vals1, vals2, rows, cols, 0, 1);

    return !((fEqual === true) && (sEqual === true));
}


/**
 * Compares two 2D arrays for equality.
 *
 * @param arr1 first array
 * @param arr2 second array
 * @param rows number of arrays rows
 * @param cols number of arrays columns
 * @param startRow specifies from which rows to check
 * @param startCol columns to check
 * @returns {boolean}
 */
function compare2DArrays(arr1, arr2, rows, cols, startRow, startCol) {
    for (let c = startRow; c < rows; c++) {
        for (let k = startCol; k < (cols - 1); k++) {
            if (arr1[c][k] !== arr2[c][k]) {
                return false;
            }
        }
    }
    return true;
}


/**
 * Rounds values of 2D array with specified precision.
 *
 * @param arr array to iterate through and set
 * @param precision precision value to set
 * @returns {Array} array with specified precision
 */
function setPrecision(arr, precision) {
    let size = arr.length;
    let res = init2DArray(size, size, 0);
    for (let c = 0; c < size; c++) {
        res[c] = roundSol(arr[c], precision);
    }

    return res;
}


/**
 * Rounds values of 1D array at given precision.
 *
 * @param arr array row to round
 * @param precision precision to round to
 * @returns {Array} array with rounded values
 */
function roundSol(arr, precision) {
    return arr.map(function (element) {
        return parseFloat(element).toFixed(precision);
    });

}


/**
 * Checks whether two values are within specified range.
 *
 * @param val first value
 * @param other second value
 * @param precision delta within which is considered equal
 * @returns {boolean} true if they are
 */
function isCorrect(val, other, precision) {
    return Math.abs(val - other) < precision;
}


/**
 * Generates two y values for points. First x coordinate is at 0 and second at cols - 1.
 *
 * @param rows number or rows in matrix for which points are generated
 * @param cols number of columns in matrix for which points are generated
 * @returns {{x0: number, x1: number, y0: number, y1: number}}
 */
function generatePoints(rows, cols) {
    let x0 = 0;
    let y0 = 0;
    let x1 = cols - 1;
    let y1 = 0;

    // Generate first point
    y0 = randNum(0, rows - 1);

    // Generate second point
    if (y0 <= 2) {
        y1 = randNum(y0 + 3, y0 + 7);
    } else if (y0 > 7) {
        y1 = randNum(y0 - 7, y0 - 3)
    } else {
        y1 = (randNum(0, 1) === 1) ? randNum(0, y0 - 3) : randNum(y0 + 3, rows - 1);
    }

    return {
        x0: x0,
        x1: x1,
        y0: y0,
        y1: y1
    }
}


/**
 * Returns a random number between min (inclusive) and max (inclusive)
 */
function randNum(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min
}