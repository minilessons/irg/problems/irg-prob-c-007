/**
 * Init every element of 2D array with given value.
 * @param rows number of rows that will be initialized
 * @param cols number of columns that will be initialized
 * @param value specifies value used to init all elements of 2D array
 * @returns {Array} 2D array of elements initialized with provided value
 */
function init2DArray(rows, cols, value) {
    let arr = [];
    for (let c = 0; c < rows; c++) {
        arr[c] = [];
        for (let k = 0; k < cols; k++) {
            arr[c][k] = value;
        }
    }
    return arr;
}