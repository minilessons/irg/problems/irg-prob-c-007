/**
 * Generates raster with specifications provided.
 * @param specs provides number of rows and columns and pixel width and height.
 * @constructor specs holds raster specification constants
 */
function Raster(specs) {
    this.rows = specs.rows;
    this.cols = specs.cols;
    this.pixelWidth = specs.pixelWidth;
    this.pixelHeight = specs.pixelHeight;

    this.pixels = init2DArray(this.rows, this.cols, 0);

    /**
     * Adds pixel to raster at position x, y in pixels array.
     * @param pixel pixel to add
     * @param x x position in raster array
     * @param y y position in raster array
     */
    this.add2Raster = function (pixel, x, y) {
        if (x > this.rows || y > this.cols) {
            throw Error("Raster Too Small!");
        }
        this.pixels[x][y] = pixel;
    };

    /**
     * Sets raster pixel to provided value.
     * @param x x coordinate to select
     * @param y x coordinate to select
     * @param selected value of selection
     */
    this.setSelection = function (x, y, selected) {
        try {
            this.pixels[x][y].shape.selected = selected;
        } catch (Error) {
            /*console.log("Pixel at: (" + x + ", " + y + ") is undefined.");
             console.log(Error);*/
        }
    };


    /**
     * Lights pixel by changing its color to specified.
     * @param x raster position of pixel to light
     * @param y raster position of pixel to light
     * @param color specifies which color to use
     */
    this.lightPixel = function (x, y, color) {
        try {
            this.pixels[x][y].shape.fill = color;
        } catch (Error) {
            throw "Pixel at: (" + x + ", " + y + ") is undefined."
        }
    };

    /**
     * Sets inner text for a pixel.
     * @param x coordinate of a pixel
     * @param y coordinate of a pixel
     * @param value text to set
     */
    this.setPixelValue = function (x, y, value) {
        try {
            this.pixels[x][y].textField.fieldText = value;
        } catch (Error) {
            throw "Pixel at: (" + x + ", " + y + ") is undefined.";
        }

    };

    /**
     * Exports pixels properties to an object.
     * @returns {{selectedPixels: Array, pixelValues: Array}}
     */
    this.exportPixels = function () {
        let rows = this.rows;
        let cols = this.cols;
        let selectedPixels = init2DArray(rows, cols, 0);
        let pixelValues = init2DArray(rows, cols, "");

        for (let c = 0; c < rows; c++) {
            selectedPixels[c] = this.pixels[c].map(function (pixel) {
                return pixel.shape.selected;
            });
            pixelValues[c] = this.pixels[c].map(function (pixel) {
                return pixel.textField.fieldText;
            });
        }
        return {
            selectedPixels: selectedPixels,
            pixelValues: pixelValues
        };
    };

    /**
     * Converts coordinates to raster coordinates.
     * @param x coordinate to convert
     * @param y coordinate to convert
     * @returns {{x: *, y: *}} swapped coordinates
     */
    this.convertToRaster = function (x, y) {
        //noinspection JSSuspiciousNameCombination
        return {
            x: y,
            y: x
        };
    };


    /**
     * Sets pixels color.
     * @param x coordinate
     * @param y coordinate
     * @param color specifies color
     */
    this.setPixelColor = function (x, y, color) {
        this.pixels[x][y].shape.fill = color;
    };

    /**
     * Gets pixel specified at by given coordinates.
     * @param x coordinate
     * @param y coordinate
     * @returns {*} pixel at coordinates
     */
    this.getPixel = function (x, y) {
        return this.pixels[x][y];
    };

    /**
     * Gets array of pixels.
     * @returns {Array|*} pixels array
     */
    this.getPixels = function () {
        return this.pixels;
    };

    /**
     * Gets text field of a pixel specified at coordinates.
     * @param x coordinate
     * @param y coordinate
     * @returns {*} pixel textField at coordinates
     */
    this.getTextField = function (x, y) {
        return this.getPixel(x, y).textField;
    };

    /**
     * Gets shape object of pixel specified at coordinates.
     * @param x coordinate
     * @param y coordinate
     * @returns {*|string} shape object at coordinates
     */
    this.getShape = function (x, y) {
        return this.getPixel(x, y).shape;
    };

    /**
     * Returns value of textField of a pixel at specified coordinates.
     * @param x coordinate
     * @param y coordinate
     * @returns {*} textField value
     */
    this.getPixelValue = function (x, y) {
        return this.getPixel(x, y).textField.fieldText;
    };

    /**
     * Gets number of pixel rows.
     * @returns {*|HTMLCollection|SQLResultSetRowList|string|Number}
     */
    this.getRows = function () {
        return this.rows;
    };

    /**
     * Gets number of pixel columns.
     * @returns {*|string|Number}
     */
    this.getCols = function () {
        return this.cols;
    };

    /**
     * Gets pixel width for this raster.
     * @returns {*} pixel width
     */
    this.getPixelWidth = function () {
        return this.pixelWidth;
    };

    /**
     * Gets pixel height for this raster.
     * @returns {*} pixel height
     */
    this.getPixelHeight = function () {
        return this.pixelHeight;
    };
}