/**
 * Canvas state represents an object which holds all information about canvas.
 * Does translations, scaling and drawing of object by itself. Can be modified in any way.
 *
 * @param canvas holds HTML canvas object
 * @param stateOpts object which holds various properties for this canvas
 * @param constants constants used by canvas
 * @param raster reference to raster which is used to manipulate pixels on screen
 * @constructor
 */
function CanvasState(canvas, stateOpts, constants, raster) {
    // Common properties
    this.canvas = canvas;
    this.width = canvas.width;
    this.height = canvas.height;
    this.ctx = canvas.getContext('2d');
    this.stateOptions = stateOpts;
    this.constants = constants;

    // Properties for tracking canvas state and canvas objects
    this.valid = false;
    this.raster = raster;
    this.shapes = [];
    this.lines = [];

    // Reference to CanvasState
    let myState = this;

    // Add Canvas interaction only if canvas is allowed to change.
    if (stateOpts.readOnly === false) {
        this.addEventListeners(myState);
    }
}

