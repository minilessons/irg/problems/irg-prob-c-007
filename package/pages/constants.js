/**
 * Main constants class for this project.
 * This constructor holds default values for all constants.
 *
 * Several constants object are provided: "colors", "fonts", "strings", "numeric" and "keyCodes". They offer various elements including a lot of colors, fonts, string constants, numeric constants and common key codes for keyboard presses.
 */
class Constants {
    constructor(colors = {
                    black: "black",
                    yellow: "yellow",
                    red: "red",
                    white: "white",
                    orange: "orange",
                    awesome: "#8836ba",
                    gray: "#AF1E37",
                    darkRed: "#d02b2a",
                    andColor: "#ABB7B7",
                    purple: "#8e44ad",
                    emerald: "#2ecc71",
                    midnight: "#2c3e50",
                    dodgerBlue: "#1E90FF",
                    flipD: "#1E90FF",
                    flipT: "#886F98",
                    diagramArrow: "#da0000",
                    flipSR: "#CB3A34",
                    flipJK: "#2ecc71",
                    registerColor: "#50928c",
                    clickBoxColor: "#746dc5",
                    darkBlue: "#00008B",
                    decoderColor: "#d4d4d6",
                    decoderTextColor: "#00017f",
                    lightSkyBlue: "#87CEFA",
                    transparentLineActive: "rgba(65, 245, 71, 0.5)",
                    transparentLinePassive: "rgba(0, 0, 0, 0.5)",
                    glowColor: "rgba(255, 0, 0, 0.5)",
                    fullTransparent: "rgba(255, 255, 255, 0)",
                    diagramBackground1: "#d6d4dd",
                    diagramBackground2: "#8cb1bf",
                    andSyncColor: "#e9e0dd",
                    // defaultPixel: "#b5e2cb",
                    defaultPixel: "#6083a2",
                    // selectedPixel: "#b65bab",
                    selectedPixel: "#e9e12e",
                    correctColor: "#71bf9a",
                    successBox: "#8cdf7b",
                    infoNiceBlue: "#2196F3",
                    // pixelActiveInput: "#df120e",
                    // startPoints: "#e23e41",
                    pixelActiveInput: "#df241a",
                    // startPoints: "#fb585c",
                    startPoints: "#e11d23",
                    selectorColor: "#12ACBD"
                },
                fonts = {
                    pixelFont: "14px bold Schoolbook",
                    normalFont: "14px Schoolbook",
                    arialFont: "12px Arial",
                    startEndPoints: "24px Schoolbook",
                    axesFont: "18px Schoolbook"
                },
                strings = {
                    startPoint: "T",
                    endPoint: "T",
                    removeBarText: "Ukloni liniju s pogreškama",
                    addBarText: " Prikaži liniju s pogreškama"
                },
                numeric = {
                    pixelValuePrecision: 3
                },
                keyCodes = {
                    left: 37,
                    up: 38,
                    right: 39,
                    down: 40,
                    enter: 13,
                    E: 69

                }) {
        this.colors = colors;
        this.fonts = fonts;
        this.strings = strings;
        this.numeric = numeric;
        this.keyCodes = keyCodes;

        /* Various text subscripts and superscripts */
        /* Subscript <sub> x </sub> numbers */
        this.sup0 = '\u2070';
        this.sup1 = '\u00B9';
        this.sup2 = '\u00B2';
        this.sup3 = '\u00B3';
        this.sup4 = '\u2074';
        this.sup5 = '\u2075';
        this.sup6 = '\u2076';
        this.sup7 = '\u2077';
        this.sup8 = '\u2078';
        this.sup9 = '\u2079';

        /* Superscript <sup> x </sup> numbers */
        this.sub0 = '\u2080';
        this.sub1 = '\u2081';
        this.sub2 = '\u2082';
        this.sub3 = '\u2083';
        this.sub4 = '\u2084';
        this.sub5 = '\u2085';
        this.sub6 = '\u2086';
        this.sub7 = '\u2087';
        this.sub8 = '\u2088';
        this.sub9 = '\u2089';

        // Small sub letters
        this.subA = '\u2090';
        this.subE = '\u2091';
        this.subO = '\u2092';
        this.subS = '\u209B';
        this.subM = '\u2098';
        this.subN = '\u2099';

        this.supN = '\u207F';

        // Other signs
        this.subPlus = '\u208A';
        this.subMinus = '\u208B';
        this.subBracketLeft = '\u208F';
        this.supBracketLeft = '\u207D';
        this.subBracketRight = '\u208E';
        this.supBracketRight = '\u207E';
    }
}


/**
 * Gets current project constants using additional specified constants in specs as well as other constants.
 *
 * @param specs specs with few additional constants
 * @returns {Constants} Constants with specified constants
 */
function getQuestionConstants(specs) {
    let colors = {
        black: "black",
        yellow: "yellow",
        red: "red",
        white: "white",
        orange: "orange",
        awesome: "#8836ba",
        gray: "#AF1E37",
        darkRed: "#d02b2a",
        andColor: "#ABB7B7",
        purple: "#8e44ad",
        emerald: "#2ecc71",
        midnight: "#2c3e50",
        dodgerBlue: "#1E90FF",
        flipD: "#1E90FF",
        flipT: "#886F98",
        diagramArrow: "#da0000",
        flipSR: "#CB3A34",
        flipJK: "#2ecc71",
        registerColor: "#50928c",
        clickBoxColor: "#746dc5",
        darkBlue: "#00008B",
        decoderColor: "#d4d4d6",
        decoderTextColor: "#00017f",
        lightSkyBlue: "#87CEFA",
        transparentLineActive: "rgba(65, 245, 71, 0.5)",
        transparentLinePassive: "rgba(0, 0, 0, 0.5)",
        glowColor: "rgba(255, 0, 0, 0.5)",
        fullTransparent: "rgba(255, 255, 255, 0)",
        diagramBackground1: "#d6d4dd",
        diagramBackground2: "#8cb1bf",
        andSyncColor: "#e9e0dd",
        // defaultPixel: "#b5e2cb",
        defaultPixel: "#6083a2",
        // selectedPixel: "#b65bab",
        selectedPixel: "#e9e12e",
        correctColor: "#71bf9a",
        successBox: "#8cdf7b",
        infoNiceBlue: "#2196F3",
        // pixelActiveInput: "#df120e",
        // startPoints: "#e23e41",
        pixelActiveInput: "#df241a",
        // startPoints: "#fb585c",
        startPoints: "#e11d23",
        selectorColor: "#12ACBD"
    };
    let fonts = {
        pixelFont: "14px bold Schoolbook",
        normalFont: "14px Schoolbook",
        arialFont: "12px Arial",
        startEndPoints: "24px Schoolbook",
        axesFont: "18px Schoolbook"
    };
    let strings = {
        startPoint: "T",
        endPoint: "T",
        removeBarText: "Ukloni liniju s pogreškama",
        addBarText: " Prikaži liniju s pogreškama"
    };
    let numeric = {
        pixelValuePrecision: 3,
        canvasWidth: specs.canWidth,
        canvasHeight: specs.canHeight
    };
    let keyCodes = {
        left: 37,
        up: 38,
        right: 39,
        down: 40,
        enter: 13,
        E: 69

    };

    return new Constants(colors, fonts, strings, numeric, keyCodes);
}