/**
 * Prevents right clicking on canvas which is popping up dialog for browser right click.
 *
 * @param e passed event object
 */
function rightClickHot(e) {
    e.preventDefault();
}


/**
 * Event listener for preventing double clicking which is selecting text outside canvas.
 *
 * @param e event object
 * @returns {boolean}
 */
function doubleClickPrevent(e) {
    e.preventDefault();
    return false;
}


/**
 * Draws pixels at specified canvas
 *
 * @param canvas canvas to draw pixels to
 */
function init(canvas) {
    let r = canvas.raster;
    let xOff = 2;
    let yOff = 2;
    pixelsDrawer(canvas, r.rows, r.cols, r.pixelWidth, r.pixelHeight, xOff, yOff);
}


/**
 * Helper function which draws text at specified position given by provided object
 *
 * @param can canvas to draw to
 * @param element object holding position of text which is draw
 * @param text text to draw
 */
function drawText(can, element, text) {
    let ctx = can.ctx;
    let canConst = can.constants;

    let textF = new TextField(text, element.x, element.y, 1, 1, canConst.fonts.axesFont);

    ctx.beginPath();
    textF.draw(ctx);
}


/**
 * Function which draws axes for raster pixels, horizontal and vertical numbers beside raster.
 *
 * @param canvas canvas to draw to
 */
function drawAxes(canvas) {
    // x axis
    let raster = canvas.raster;
    let rows = raster.rows;
    let cols = raster.cols;
    let offset = 1.3;
    for (let c = 0; c < cols; c++) {
        let p = raster.getShape(0, c);
        let e = {
            x: p.x + p.w / 2,
            y: p.y + offset * p.h
        };
        drawText(canvas, e, c.toString());
    }

    // y axis
    for (let c = 0; c < rows; c++) {
        let p = raster.getShape(c, 1);
        let e = {
            x: p.x - offset * p.w,
            y: p.y + p.h / 2
        };
        drawText(canvas, e, c.toString());
    }
}


/**
 * Draws pixels in specific canvas
 *
 * @param canvas canvas to draw pixels to
 * @param rows number of pixel rows
 * @param cols number of pixel columns
 * @param w width of a pixel
 * @param h height of a pixel
 * @param xOff offset from one to another pixel in x dimension
 * @param yOff offset from one to another pixel in y dimension
 */
function pixelsDrawer(canvas, rows = 10, cols = 10, w = 35, h = 35, xOff = 0, yOff = 0) {
    let canConst = canvas.constants;
    let opts = canvas.stateOptions;
    let setup = opts.initialSetup;

    let onePixelHeight = h + yOff;
    let totalRasterWidth = cols * (w + xOff);
    let totalRasterHeight = rows * (h + yOff);
    let centerX = (canvas.width - totalRasterWidth) / 2;
    let centerY = ((canvas.height - totalRasterHeight) / 2) + totalRasterHeight - onePixelHeight;
    for (let c = 0; c < rows; c++) {
        for (let k = 0; k < cols; k++) {
            let x = centerX + k * (w + xOff);
            let y = centerY - c * (h + yOff);
            let condVal = setup.pixelValues[c][k];
            let setValue = "";
            if (opts.enableText === true) {
                setValue =
                    ((isNaN(condVal) === true)
                        || (condVal === "")) ? "" : setup.pixelValues[c][k];
            }

            let setSelection = setup.selectedPixels[c][k];
            let selectionColor = (setSelection === 1) ?
                canConst.colors.selectedPixel : canConst.colors.defaultPixel;

            // Create TextField object
            let textField = new TextField(
                setValue, x, y, w, h, canConst.fonts.pixelFont, canConst.colors.black);

            // Create Shape object
            let shape = new Shape(x, y, w, h, selectionColor, setSelection);

            // Create pixel from TextField and Shape objects
            let pixel = new Pixel(shape, textField);
            canvas.raster.add2Raster(pixel, c, k);
        }
    }
}


/**
 * Initializes starting points for line segment at specified canvas
 *
 * @param canvas canvas to draw points to
 * @param p point coordinates in pixel values
 */
function initStartPoints(canvas, p) {
    let raster = canvas.raster;
    let canConst = canvas.constants;
    let startPoint = raster.convertToRaster(p.x0, p.y0);
    let endPoint = raster.convertToRaster(p.x1, p.y1);

    raster.getTextField(startPoint.x, startPoint.y).font = canConst.fonts.startEndPoints;
    raster.getTextField(endPoint.x, endPoint.y).font = canConst.fonts.startEndPoints;

    raster.lightPixel(startPoint.x, startPoint.y, canConst.colors.startPoints);
    raster.setPixelValue(startPoint.x, startPoint.y, canConst.strings.startPoint + canConst.subS);
    raster.lightPixel(endPoint.x, endPoint.y, canConst.colors.startPoints);
    raster.setPixelValue(endPoint.x, endPoint.y, canConst.strings.startPoint + canConst.subE);
}


/**
 * Gets mouse coordinates
 *
 * @param canvas canvas in which mouse is at
 * @param e event object
 * @returns {{x: (number|*), y: (number|*)}}
 */
function getMouse(canvas, e) {
    let rect = canvas.getBoundingClientRect();
    let pos = {
        x: e.clientX - rect.left,
        y: e.clientY - rect.top
    };

    return {x: pos.x, y: pos.y}
}


/**
 * Checks wheter pixel is clicked given mouse coordinates and canvas state
 *
 * @param canvas canvas used to check if any element is clicked in it
 * @param mouse mouse coordinates
 * @returns {{x: number, y: number}}
 */
function isClicked(canvas, mouse) {
    let raster = canvas.raster;
    let index = {x: -1, y: -1};
    let rows = raster.rows;
    let cols = raster.cols;
    for (let c = 0; c < rows; c++) {
        for (let k = 0; k < cols; k++) {
            if (raster.getPixel(c, k).contains(mouse.x, mouse.y)) {
                // Remember index
                if ((k !== 0) && (k !== cols - 1)) {
                    index.x = c;
                    index.y = k;
                }
            }
        }
    }
    return index;
}
