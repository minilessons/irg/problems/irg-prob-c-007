/**
 * Sets html canvas properties from constants.
 *
 * @param canvas canvas for which properties are set
 * @param constants constants
 */
function setCanvasProperties(canvas, constants) {
    canvas.width = constants.numeric.canvasWidth;
    canvas.height = constants.numeric.canvasHeight;
}


/**
 * Creates <span> tags for <pre> code in html.
 *
 * @param localID local question ID
 */
function createCodeSpans(localID) {
    let firstId = localID + "_codePos";
    let secondId = localID + "_codeNeg";

    let preFirst = document.getElementById(firstId);
    let preSecond = document.getElementById(secondId);

    let classId = "lineNumber";
    addSpanToPre(preFirst, classId);
    addSpanToPre(preSecond, classId);

}


/**
 * Highlights specific keywords for one line of code.
 *
 * @param line line of code to apply highlighting
 * @returns highlighted line of code
 */
function highlightKeywords(line) {
    let keywords = ["double", "int"];
    let highlightedLine = line;
    keywords.forEach(keyword => {
        let items = line.split(keyword);
        if (items.length === 2) {
            highlightedLine = items[0] + "<span style='color:#1ac340'>" + keyword + "</span>" + items[1];
        }
    });
    return highlightedLine;
}


/**
 * Helper function to highlight function names.
 *
 * @param line line of code to highlight function names
 * @returns {string} highlighted line
 */
function highlightFunctions(line) {
    let putPixelFunc = "putPixel";

    let highlightedLine = line;
    let items = line.split(putPixelFunc);
    if (items.length === 2) {
        highlightedLine = items[0] + "<span style='color:orange'>" + putPixelFunc + "</span>" + items[1];
    }

    return highlightedLine;
}


/**
 * Adds spans to specific <pre> tag.
 *
 * @param pre tag to add spans to
 * @param classId identification to add to span tags.
 */
function addSpanToPre(pre, classId) {
    let lines = pre.innerHTML.split(/\n/);
    let numOfLines = lines.length;
    let linesWithSpan = "";
    for (let c = 0; c < numOfLines; c++) {
        lines[c] = highlightKeywords(lines[c]);
        lines[c] = highlightFunctions(lines[c]);
        linesWithSpan += "<span class=" + classId + ">" + lines[c] + "</span>" + "\n";
    }
    pre.innerHTML = linesWithSpan;
}
