/**
 * Main prototypes for CanvasState.
 * Initializes CanvasState prototypes before their usage.
 */
function initPrototypes() {
    /**
     * Clears all objects which are used to show elements at canvas.
     */
    CanvasState.prototype.clearShapes = function () {
        this.lines = [];
        this.shapes = [];
        this.valid = false;
    };

    /**
     * Redraws whole canvas.
     */
    CanvasState.prototype.invalidate = function () {
        this.valid = false;
        this.draw();
    };

    /**
     * Adds line to canvas.
     *
     * @param line shape which is constructed with specified interface.
     */
    CanvasState.prototype.addLine = function (line) {
        this.lines.push(line);
        this.invalidate();
    };

    /**
     * Adds shape to canvas.
     *
     * @param shape shape object
     */
    CanvasState.prototype.addShape = function (shape) {
        this.shapes.push(shape);
        this.invalidate();
    };

    /**
     * Clears all canvas drawings on context which canvas represents.
     */
    CanvasState.prototype.clear = function () {
        this.ctx.clearRect(0, 0, this.width, this.height);
    };

    /**
     * Removes only shape objects elements.
     */
    CanvasState.prototype.removeShapes = function () {
        this.shapes = [];
        this.invalidate();
    };

    /**
     * Checks if canvas contains point provided.
     *
     * @param mx x coordinate of a point
     * @param my y coordinate of a point
     * @returns {boolean}
     */
    CanvasState.prototype.contains = function (mx, my) {
        let rect = this.canvas.getBoundingClientRect();
        return (mx < this.width && mx > rect.left && my > rect.top || my < rect.height);
    };


    /**
     * Draws all specified elements in this canvas. Only if they are not up to date.
     */
    CanvasState.prototype.draw = function () {
        if (!this.valid) {
            let ctx = this.ctx;
            let shapes = this.shapes;
            let lines = this.lines;
            let constants = this.constants;
            this.clear();

            // Draw shapes
            let l = shapes.length;
            for (let i = 0; i < l; i++) {
                ctx.beginPath();
                shapes[i].draw(constants, ctx);
            }

            // Draw buttons
            let raster = this.raster;
            let rows = raster.getRows();
            let cols = raster.getCols();

            for (let c = 0; c < rows; c++) {
                for (let k = 0; k < cols; k++) {
                    ctx.beginPath();
                    if (this.stateOptions.enableText === true) {
                        raster.getPixel(c, k).draw(ctx);
                    } else {
                        raster.getShape(c, k).draw(ctx);
                    }
                }
            }

            // Shapes that are always on canvas
            // Error bar lines
            let lineLength = lines.length;
            for (let c = 0; c < lineLength; c++) {
                ctx.beginPath();
                lines[c].draw(ctx);
            }
            drawAxes(this);

            this.valid = true;
        }
    };

    /**
     * Adds canvas interaction.
     *
     * @param myState reference to this canvas
     */
    CanvasState.prototype.addEventListeners = function (myState) {
        // Event listeners
        this.canvas.addEventListener("selectstart", doubleClickPrevent, false);
        this.canvas.addEventListener("contextmenu", rightClickHot, false);
        this.canvas.addEventListener("mousedown", mouseDownCallback, true);
        this.canvas.addEventListener("dblclick", function (e) {
            myState.stateOptions.gObj.showModal(e, myState);
        }, false);


        function mouseDownCallback(e) {
            mouseClick(e, myState);
        }
    }
}


initPrototypes();