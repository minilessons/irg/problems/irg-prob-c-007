/**
 * Removes error bars from canvas and enables pixel text to be seen.
 *
 * @param can canvas to remove error bars from
 */
function removeErrorBars(can) {
    can.lines = [];
    can.stateOptions.enableText = true;
    can.invalidate();
}


/**
 * Helper function which draws line by creating new Line object and sending it to Canvas.
 *
 * @param can canvas to send the Line to
 * @param p line parameters specified as x0, y0, x1, y1
 */
function drawLine(can, p) {
    let line = new Line(p.x0, p.y0, p.x1, p.y1);
    can.addLine(line);
}


/**
 * Draws error bars on line segment initialized at the question setup.
 *
 * @param canvas canvas to draw to
 */
function drawErrorBars(canvas) {
    canvas.lines = [];
    let ctx = canvas.ctx;
    let raster = canvas.raster;
    let pixels = raster.getPixels();
    let opts = canvas.stateOptions;
    let rows = pixels.length;
    let cols = pixels[0].length;
    let points = opts.initialPoints;

    let startPoint = raster.convertToRaster(points.x0, points.y0);
    let endPoint = raster.convertToRaster(points.x1, points.y1);
    let st = raster.getShape(startPoint.x, startPoint.y);
    let end = raster.getShape(endPoint.x, endPoint.y);
    let l = {
        x0: st.x + st.w / 2,
        y0: st.y + st.h / 2,
        x1: end.x + end.w / 2,
        y1: end.y + end.h / 2
    };

    ctx.save();
    ctx.lineCap = "round";
    ctx.lineWidth = 3;
    drawLine(canvas, l);

    for (let c = 0; c < rows; c++) {
        for (let k = 0; k < cols; k++) {
            let sh = raster.getShape(c, k);
            if (sh.selected === 1) {
                // Calculate btn center
                let centerX = sh.x + sh.w / 2;
                let centerY = sh.y + sh.h / 2;

                // Intersection
                let a = (l.y1 - l.y0) / (l.x1 - l.x0);
                let b = l.y0 - a * l.x0;
                let intersect = a * centerX + b;

                let p = {
                    x0: centerX,
                    y0: centerY,
                    x1: centerX,
                    y1: intersect
                };
                drawLine(canvas, p)
            }
        }
    }
    ctx.restore();
}


/**
 * Helper function for closing dialog by identification.
 *
 * @param ID id of an element to close
 */
function closeDialogByID(ID) {
    closeDivByID(ID);
}


/**
 * Helper function to open dialog by identification.
 *
 * @param ID id of an element to show
 */
function openDialogByID(ID) {
    showDivByID(ID);
}


/**
 * Shows div tag element with specified identification.
 *
 * @param ID id of an element to show
 */
function showDivByID(ID) {
    document.getElementById(ID).style.display = "block";
}


/**
 * Hides div tag element with specified identification.
 *
 * @param ID id of an element to hide
 */
function closeDivByID(ID) {
    document.getElementById(ID).style.display = "none";
}


/**
 * Sets focus for element specified by identification.
 *
 * @param ID id of an element to set focus to
 */
function setFocusById(ID) {
    document.getElementById(ID).focus();
}
