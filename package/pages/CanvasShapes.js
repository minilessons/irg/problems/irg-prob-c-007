/**
 * Function constructor which initializes a Shape.
 *
 * @param x top left x coordinate of a shape
 * @param y top left y coordinate of a shape
 * @param w width of the shape
 * @param h height of the shape
 * @param fill shape fill
 * @param selected simulates selection of this shape
 * @constructor
 */
function Shape(x, y, w, h, fill, selected) {
    this.x = x || 0;
    this.y = y || 0;
    this.w = w || 1;
    this.h = h || 1;
    this.fill = fill || '#AAAAAA';
    this.show = true;
    this.selected = selected || 0;
}


/**
 * Function constructor for line shape.
 *
 * @param x1 start x value of this line
 * @param y1 start y value of this line
 * @param x2 end x value of this line
 * @param y2 end y value of this line
 * @param color line color
 * @param fill line fill
 * @param lineWidth width of the line
 * @constructor
 */
function Line(x1, y1, x2, y2, color, fill, lineWidth) {
    this.x1 = x1 || 0;
    this.x2 = x2 || 0;
    this.y1 = y1 || 1;
    this.y2 = y2 || 1;
    this.color = color || "#000000";
    this.fill = fill || "#000000";
    this.lineWidth = lineWidth || 2;
}


/**
 * Function constructor for text field.
 *
 * @param text text which is shown in text field
 * @param x top left x coordinate for text field
 * @param y top left y coordinate for text field
 * @param w width of the field
 * @param h height of the field
 * @param font font to use for text showcase
 * @param fillStyle fill to use on text field
 * @param textColor color of the text
 * @param textAlign text alignment
 * @param baseline text baseline
 * @param isStroke stroked or filled text: true and false respectively
 * @constructor
 */
function TextField(text, x, y, w, h, font, fillStyle, textColor, textAlign, baseline, isStroke = false) {
    this.x = x || 0;
    this.y = y || 0;
    this.w = w || 1;
    this.h = h || 1;
    this.fillStyle = fillStyle || "#000000";
    this.textBaseline = baseline || "middle";
    this.textColor = textColor || "#000000";
    this.font = font || "12px Courier";
    this.fieldText = text || "";
    this.textAlign = textAlign || "center";
    this.isStroke = isStroke || false;
}


/**
 * Function constructor for circle shape.
 *
 * @param centerX x value for center of a circle
 * @param centerY y value for center of a circle
 * @param args arguments which are used to init circle common values
 * @constructor
 */
function SimpleCircle(centerX, centerY, args) {
    this.x = centerX || 0;
    this.y = centerY || 0;
    this.radius = args.radius || Math.PI;
    this.startAngle = args.startAngle || 0;
    this.endAngle = args.endAngle || 360;
    this.rotation = args.rotation || false;
    this.color = args.color || "#000000";
    this.fill = args.fillStyle || "#000000";
    this.lineWidth = args.lineWidth || 2;
}


/**
 * Generates pixel from rectangle and text field.
 *
 * @param shape used to generate rectangle shape of a pixel
 * @param textField text inside shape, specified by another object "TextField"
 * @constructor
 */
function Pixel(shape, textField) {
    this.shape = shape || undefined;
    this.textField = textField || undefined;
}


/**
 * Draws pixel by drawing shape and textField.
 *
 * @param ctx context to draw to
 */
Pixel.prototype.draw = function (ctx) {
    this.shape.draw(ctx);
    this.textField.draw(ctx);
};

/**
 * Checks whether Pixel contains given point
 *
 * @param x x coordinate of a point
 * @param y y coordinate of a point
 * @returns {boolean}
 */
Pixel.prototype.contains = function (x, y) {
    return this.shape.contains(x, y);
};

/**
 * Prototype which is used for drawing text at a shape.
 *
 * @param can canvas to draw to
 */
Shape.prototype.drawText = function (can) {
    let ctx = can.ctx;
    let s = can.stateOptions;

    // Prepare Text
    if (s.enableText === true) {
        prepareText(ctx, this.textField, this.x, this.y, this.w, this.h);
    }
};


/**
 * Prototype which is used to draw this shape object.
 *
 * @param ctx context to draw to
 */
Shape.prototype.draw = function (ctx) {
    ctx.save();
    ctx.beginPath();
    ctx.fillStyle = this.fill;
    ctx.fillRect(this.x, this.y, this.w, this.h);
    ctx.stroke();
    ctx.restore();
};


/**
 * Helper function which fills text at specified context
 *
 * @param ctx context to draw text to
 * @param textField text field representing all needed values for drawing text
 * @param x1 top left x value of text field
 * @param y1 top left y value of text field
 * @param w text field width
 * @param h text field height
 * @param centered is text in the center of text field; default: true
 */
function prepareText(ctx, textField, x1, y1, w, h, centered = true) {
    // Set text in the center
    ctx.save();
    let textX = (centered === true) ? x1 + (w / 2) : x1;
    let textY = (centered === true) ? y1 + (h / 2) : y1;
    ctx.font = textField.font;
    ctx.textAlign = (centered === true) ? "center" : "left";
    ctx.textBaseline = "middle";
    ctx.strokeStyle = "#161314";
    ctx.fillStyle = textField.fill;
    ctx.fillText(textField.fieldText, textX, textY);
    ctx.restore();
}


/**
 * Prototype which is used to draw Line object.
 *
 * @param ctx context to draw to
 */
Line.prototype.draw = function (ctx) {
    ctx.save();
    ctx.strokeStyle = this.fill;
    ctx.color = this.color;
    ctx.lineWidth = this.lineWidth;
    ctx.moveTo(this.x1, this.y1);
    ctx.lineTo(this.x2, this.y2);
    ctx.stroke();
    ctx.restore();
};

/**
 * Prototype which is used to draw TextField object.
 *
 * @param ctx context to draw to
 */
TextField.prototype.draw = function (ctx) {
    ctx.save();
    ctx.textAlign = this.textAlign || ctx.textAlign;
    ctx.textBaseline = this.textBaseline || ctx.textBaseline;
    ctx.strokeStyle = this.strokeStyle || ctx.strokeStyle;
    ctx.fillStyle = this.fillStyle || ctx.fillStyle;
    ctx.font = this.font || ctx.font;

    let textX = (this.textAlign === "center") ? this.x + (this.w / 2) : this.x;
    let textY = (this.textAlign === "center") ? this.y + (this.h / 2) : this.y;

    if (this.isStroke === true) {
        ctx.strokeText(this.fieldText, textX, textY);
        ctx.stroke();
    } else {
        ctx.fillText(this.fieldText, textX, textY);
        ctx.stroke();
    }
    ctx.restore();
};

/**
 * Prototype which is used to draw SimpleCircle object.
 *
 * @param ctx context to draw to
 */
SimpleCircle.prototype.draw = function (ctx) {
    ctx.save();
    ctx.strokeStyle = this.color;
    ctx.fillStyle = this.fill;
    ctx.lineWidth = this.lineWidth;
    ctx.arc(this.x, this.y, this.radius, this.startAngle, this.endAngle, this.rotation);
    ctx.stroke();
    ctx.fill();
    ctx.restore();
};


/**
 * Checks if point where mouse points is contained in this object (Circle).
 *
 * @param mx x coordinate of a mouse
 * @param my y coordinate of a mouse
 * @returns {boolean} true if point is contained
 */
SimpleCircle.prototype.contains = function (mx, my) {
    let r = this.radius;
    let deltaX = mx - this.x;
    let deltaY = mx - this.y;
    return (deltaX * deltaX) + (deltaY * deltaY) < (r * r);
};

/**
 * Checks whether TextField object contains point where mouse is at.
 *
 * @param mx x coordinate of a mouse
 * @param my y coordinate of a mouse
 * @returns {boolean} true if point is contained
 */
TextField.prototype.contains = function (mx, my) {
    return (this.x <= mx) && (this.x + this.w >= mx) &&
        (this.y <= my) && (this.y + this.h >= my);
};

/**
 * Checks whether Line object contains point where mouse is at.
 *
 * @param mx x coordinate of a mouse
 * @param my y coordinate of a mouse
 * @returns {boolean} true if point is contained
 */
Line.prototype.contains = function (mx, my) {
    return (this.x <= mx) && (this.x + this.w >= mx) &&
        (this.y <= my) && (this.y + this.h >= my);
};

/**
 * Checks whether Shape object contains point where mouse is at.
 *
 * @param mx x coordinate of a mouse
 * @param my y coordinate of a mouse
 * @returns {boolean} true if point is contained
 */
Shape.prototype.contains = function (mx, my) {
    return (this.x <= mx) && (this.x + this.w >= mx) &&
        (this.y <= my) && (this.y + this.h >= my);
};