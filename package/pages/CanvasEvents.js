/**
 * Mouse click handler for canvasState: myState
 *
 * @param e event object
 * @param myState canvasState
 */
function mouseClick(e, myState) {
    let canvas = myState.canvas;

    let mouse = getMouse(canvas, e);
    let raster = myState.raster;

    // If user clicked on any pixel set its properties accordingly
    let idx = isClicked(myState, mouse);
    if ((idx.x !== -1) && (idx.y !== -1)) {
        let sh = raster.getShape(idx.x, idx.y);
        sh.selected = sh.selected === 1 ? 0 : 1;
        if (sh.selected === 0) {
            sh.fill = myState.constants.colors.defaultPixel;
            raster.setPixelValue(idx.x, idx.y, "");
        } else {
            sh.fill = myState.constants.colors.selectedPixel;
        }
    }

    let errId = myState.stateOptions.htmlIds.errorBar;
    let errBar = document.getElementById(errId);
    if (errBar.innerHTML === canConst.strings.removeBarText) {
        drawErrorBars(myState);
    }

    myState.invalidate();
}