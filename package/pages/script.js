/*@#include#(constants.js)*/
/*@#common-include#(commonUtil.js)*/

/*@#include#(CanvasProto.js)*/
/*@#include#(CanvasEvents.js)*/
/*@#include#(CanvasShapes.js)*/
/*@#include#(CanvasX.js)*/
/*@#include#(canvasUtil.js)*/
/*@#include#(utilityFunctions.js)*/
/*@#include#(frontendUtil.js)*/
/*@#include#(raster.js)*/


/**
 * @external questionLocalID
 * @type {string}
 */
let canId = questionLocalID + "_questionCanvas";
let errId = questionLocalID + "_errorBarBtn";
let modId = questionLocalID + "_modalDialog";
let formId = questionLocalID + "_boxForm";
let valId = questionLocalID + "_boxValue";
let inputBoxId = questionLocalID + "_inputBoxId";
let myDim = questionLocalID + "_myDim";

let canConst;
let stateOptions;
let canvasState;


/**
 * Defines global object which is exported and used in HTML document.
 *
 * @type {{closeDialog: gObj.closeDialog, openDialog: gObj.openDialog, validateForm: gObj.validateForm, toggleErrorBar: gObj.toggleErrorBar, showModal: gObj.showModal}}
 */
let gObj = {
    /**
     * Toggles element specified by html ID.
     *
     * @param id id of element to toggle
     */
    toggleElement: function (id) {
        let elem = document.getElementById(id);
        if (elem.style.display === "none") {
            showDivByID(id);
        } else {
            closeDivByID(id);
        }
    },

    /**
     * Closes modal dialog.
     */
    closeDialog: function () {
        let opts = canvasState.stateOptions;
        let raster = canvasState.raster;
        let canConst = canvasState.constants;
        raster.lightPixel(opts.index.x, opts.index.y, canConst.colors.selectedPixel);
        canvasState.invalidate();
        closeDialogByID(modId);
        closeDivByID(myDim);
    },

    /**
     * Shows dialog for user input and set focus on it.
     */
    openDialog: function () {
        openDialogByID(modId);
        setFocusById(inputBoxId);
        showDivByID(myDim);
    },


    /**
     * Validates user input before sending it into canvas.
     *
     * @returns {boolean} always false because of form submit.
     */
    validateForm: function () {
        let opts = canvasState.stateOptions;
        let raster = canvasState.raster;
        let canConst = canvasState.constants;

        let formVal = Number(document.forms[formId][valId].value)
            .toFixed(canConst.numeric.pixelValuePrecision);

        let idx = opts.index;
        if ((idx.x !== -1) && (idx.y !== -1)) {
            raster.lightPixel(idx.x, idx.y, canConst.colors.selectedPixel);
            raster.setPixelValue(idx.x, idx.y, formVal);
            raster.setSelection(idx.x, idx.y, 1);

            this.closeDialog();
            canvasState.invalidate();
        }
        return false;
    },

    /**
     * Toggles error bars and line between two points.
     */
    toggleErrorBar: function () {
        let opts = canvasState.stateOptions;
        let canConst = canvasState.constants;
        let errBar = document.getElementById(errId);
        if (errBar.innerHTML === canConst.strings.removeBarText) {
            errBar.innerHTML = canConst.strings.addBarText;
            removeErrorBars(canvasState);
        } else {
            errBar.innerHTML = canConst.strings.removeBarText;
            opts.enableText = false;
            drawErrorBars(canvasState);
        }
    },


    /**
     * Event callback for modal dialog.
     * Shows prepared modal dialog to user if user clicked right mouse button on pixel.
     *
     * @param canvas canvas which the user clicked
     * @param e passed event object
     */
    showModal: function (e, canvas) {
        let opts = canvas.stateOptions;
        let canConst = canvas.constants;
        let canvasObj = canvas.canvas;

        let mouse = getMouse(canvasObj, e);
        let index = isClicked(canvas, mouse);
        let raster = canvas.raster;
        if (index.x !== -1 && index.y !== -1) {
            opts.index.x = index.x;
            opts.index.y = index.y;
            raster.setPixelColor(index.x, index.y, canConst.colors.pixelActiveInput);
            canvas.invalidate();

            let setupVal = Number(
                raster.getPixelValue(index.x, index.y));
            if (isNaN(setupVal)) {
                setupVal = 0.000;
            }
            document.forms[formId][valId].value = setupVal;
            this.openDialog();
        }
    }
};


/**
 * Global Export with name "gObj"
 *
 * @external qExportGlobalObject
 */
qExportGlobalObject(questionLocalID, 'gObj', gObj);

/**
 * Api used to communicate between html and backend data stream
 *
 * @type {{initQuestion: api.initQuestion, takeState: api.takeState, resetState: api.resetState, revertState: api.revertState}}
 */
let api = {
    /**
     * Initializes question with user state and common state
     *
     * @param cst common state object
     * @param userState user data object
     */
    initQuestion: function (cst, userState) {
        /**
         * @external readOnly
         */
        // Hide how to use and description in readOnly mode
        if (readOnly === true) {
            gObj.toggleElement(questionLocalID + "_taskDesc");
            gObj.toggleElement(questionLocalID + "_htuDesc");
            gObj.toggleElement(questionLocalID + "_codePos");
            gObj.toggleElement(questionLocalID + "_codeNeg");
        }

        // Add needed span tags for <pre> codes.
        createCodeSpans(questionLocalID);

        canConst = getQuestionConstants(cst.rasterSpecs);
        let startingPixels = cst.points;

        // Init raster
        let raster = new Raster(cst.rasterSpecs);

        stateOptions = {
            initialSetup: userState.qs,
            curY0: -1,
            curYE: -1,
            index: {x: -1, y: -1},
            formIndex: {x: 0, y: 0},
            readOnly: readOnly,
            enableText: true,
            initialPoints: startingPixels,
            gObj: gObj,
            htmlIds: {
                errorBar: errId
            }
        };

        let canvas = document.getElementById(canId);
        setCanvasProperties(canvas, canConst);
        canvasState =
            new CanvasState(canvas, stateOptions, canConst, raster);

        init(canvasState);
        initStartPoints(canvasState, startingPixels);
        canvasState.invalidate();
        document.getElementById(errId).style.display = "block";

    },

    /**
     * Export function for taking current and last user entered data
     *
     * @returns {{selectedPixels: (*|Array), pixelValues: (*|Array)}}
     */
    takeState: function () {
        return canvasState.raster.exportPixels();
    },

    /**
     * Reset function for task redoing.
     *
     * @param cst common state
     * @param emptyState empty state to reset to
     */
    resetState: function (cst, emptyState) {
        canvasState.stateOptions.initialSetup = emptyState.qs;
        canvasState.clearShapes();
        canvasState.stateOptions.enableText = true;
        document.getElementById(errId).innerHTML = canvasState.constants.strings.addBarText;
        init(canvasState);
        initStartPoints(canvasState, cst.points);
        canvasState.invalidate();
    },

    /**
     * Reverts the question state to a state which was loaded when page loaded.
     *
     * @param cst common state
     * @param loadedState state at the time of page load
     */
    revertState: function (cst, loadedState) {
        // Reset state and options
        canvasState.stateOptions.initialSetup = loadedState.qs;
        canvasState.stateOptions.enableText = true;
        document.getElementById(errId).innerHTML = canvasState.constants.strings.addBarText;
        canvasState.clearShapes();

        init(canvasState);
        initStartPoints(canvasState, cst.points);
        canvasState.invalidate();
    }
};

/**
 * Closes modal dialog if user clicks anywhere outside the modal dialog.
 *
 * @param e passed event object
 */
window.onclick = function (e) {
    let modal = document.getElementById(modId);
    if (e.target === modal) {
        gObj.closeDialog();
    }
};


// noinspection JSAnnotator
return api;